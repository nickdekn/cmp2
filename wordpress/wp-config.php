<?php
/**
 * The base configuration for WordPress
 *
 * The wp-config.php creation script uses this file during the
 * installation. You don't have to use the web site, you can
 * copy this file to "wp-config.php" and fill in the values.
 *
 * This file contains the following configurations:
 *
 * * MySQL settings
 * * Secret keys
 * * Database table prefix
 * * ABSPATH
 *
 * @link https://codex.wordpress.org/Editing_wp-config.php
 *
 * @package WordPress
 */

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define('DB_NAME', 'cmp2');

/** MySQL database username */
define('DB_USER', 'root');

/** MySQL database password */
define('DB_PASSWORD', 'bitnami');

/** MySQL hostname */
define('DB_HOST', 'localhost');

/** Database Charset to use in creating database tables. */
define('DB_CHARSET', 'utf8mb4');

/** The Database Collate type. Don't change this if in doubt. */
define('DB_COLLATE', '');

/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define('AUTH_KEY',         '5(BK/O&SHC`sqssqoQAOhZGj5szhV$SKsX|EkI7rO?BAy_l$p>|7!$4%}Ae)PGi4');
define('SECURE_AUTH_KEY',  's5rvS9o wIgZOt!43-Srb+o9G?B120nB}d<UWEHg8Du2Kbww#j4c_[K|gUrk?RFg');
define('LOGGED_IN_KEY',    'b#Uy;DK(gn}iL0L4U6Xf{g7WzKrpAysB(7Db^30>JfNWfG2rtwL@:}sjPh(Y!!)Q');
define('NONCE_KEY',        '@jmzxmsI8$3_0Xmk@c601x+9C~Cm6gekDF/m-{LP%vPb6%*G*gAYuWJ4IS7UTirX');
define('AUTH_SALT',        '.D+<GDZ5X=[<;F>?f:#/CH<.Zj)j&qt&S)AOrDb7(2WGfH@flljV(l#Dk|Xnl>]]');
define('SECURE_AUTH_SALT', 'q].d8P*J:I2qYKU%TZVlS9*%n8r>&~i?c%BXb5n`]x8{PAvY/W#c%^pBot)P-|*T');
define('LOGGED_IN_SALT',   'gpg_R&u8=mXH4Zfe/Wv]|fqg}/={C/47kQU=YKUip_^Bj8p!3VlNr%]M7rTX]L}u');
define('NONCE_SALT',       'KWAQ]q+t{u=i]YCe(j*|_+Cgn>pwL&,&H4*G$[AG,05Nk<Jm,c;wi#v5CY*/!! `');

/**#@-*/

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each
 * a unique prefix. Only numbers, letters, and underscores please!
 */
$table_prefix  = 'wp_';

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 *
 * For information on other constants that can be used for debugging,
 * visit the Codex.
 *
 * @link https://codex.wordpress.org/Debugging_in_WordPress
 */
define('WP_DEBUG', false);

/* That's all, stop editing! Happy blogging. */

/** Absolute path to the WordPress directory. */
if ( !defined('ABSPATH') )
	define('ABSPATH', dirname(__FILE__) . '/');

/** Sets up WordPress vars and included files. */
require_once(ABSPATH . 'wp-settings.php');
