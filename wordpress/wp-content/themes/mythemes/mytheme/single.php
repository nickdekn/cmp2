<?php get_header(); ?>
<div class="main-container">
    <div class="main wrapper clearfix">
        <?php get_sidebar(); ?>
<?php

if(have_posts())
{
    while(have_posts())
    {
        the_post();
        the_date();
        the_author();
        //Print the title and the content of the current post
        the_title('<h1>', '</h1>');
        the_content();
        echo '<hr>';
    }
}
else
{
    echo 'No content available';
}

comments_template();
?>
    </div> <!-- #main -->
</div> <!-- #main-container -->

<?php get_footer(); ?>
