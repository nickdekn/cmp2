<?php get_header(); ?>

        <div class="main-container">
            <div class="main wrapper clearfix">
                <h1 id="recent">PORTFOLIO</h1>
                <hr>
                <article>
                    <?php
                    if(have_posts())
                    {
                        while(have_posts())
                        {
                            the_post();
                            //Print the title and the content of the current post
                              ?>
                            <a id="posts" href="<?php the_permalink(); ?>" title="<?php the_title() ?>">
                                <h2>
                                    <?php the_title()?>
                                </h2>
                            </a>

                            <?php
                            the_content();
                            echo '<hr>';
                            echo '<br>';
                        }
                    }
                    else
                    {
                        echo 'No content available';
                    }?>
                </article>

                <?php get_sidebar(); ?>

            </div> <!-- #main -->
        </div> <!-- #main-container -->

<?php get_footer(); ?>