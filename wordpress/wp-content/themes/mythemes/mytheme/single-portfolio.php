<?php get_header(); ?>
<div class="main-container">
    <div class="main wrapper clearfix">
        <?php get_sidebar(); ?>
<?php
if(have_posts())
{
    while(have_posts())
    {
        the_post();
        the_date();
        the_author();
        //Print the title and the content of the current post
        the_title('<h2>', '</h2>');
        the_content();
        echo '<hr>';
    }
}
else
{
    echo 'No content available';
}

echo get_post_meta(get_the_ID(), 'tekst', true);?><br><?php

$image = get_field('afbeelding');
$link = get_field('link', $image['ID']);
?>
<a href="<?php echo $link; ?>">
    <img src="<?php echo $image['sizes']['medium']; ?>" alt="<?php echo $image['alt']; ?>" />
</a>
<?php
comments_template();
?>
        </div>
    </div>
<?php get_footer(); ?>
