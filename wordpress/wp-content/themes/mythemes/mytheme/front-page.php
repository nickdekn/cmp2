<?php get_header(); ?>
    <div class="main-container">
        <div class="main wrapper clearfix">
            <h1 id="recent">Recent</h1>
            <hr>
            <article>
                <?php
                $postslist = get_posts('numberposts=3');
                foreach ($postslist as $post) :
                    setup_postdata($post);
                    ?>
                    <div class="post">
                        <h2><a id="posts" href="<?php the_permalink(); ?>" title="<?php the_title(); ?>">
                                <?php the_title(); ?></a>
                        </h2>
                        <p>
                            Auteur: <?php the_author(); ?><br>
                            Datum: <?php the_date(); ?> om
                            <?php the_time(); ?>
                        </p>
                        <?php the_content();
                        echo '<hr>'; ?>
                    </div>
                <?php endforeach ?>
            </article>
            <?php get_sidebar(); ?>
        </div> <!-- #main -->
    </div> <!-- #main-container -->
<?php get_footer(); ?>