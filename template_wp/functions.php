<?php
/**
 * Created by PhpStorm.
 * User: nicky
 * Date: 24/02/2016
 * Time: 10:17
 */


    function register_menu_locations() {
        register_nav_menus(
            array(
                'primary-menu' => __( 'Primary Menu' ),
                'footer-menu' => __( 'Footer Menu' )
            )
        );
    }
add_action( 'init', 'register_menu_locations' );

function register_sidebar_locations() {
    /* Register the 'primary' sidebar. */
    register_sidebar(
        array(
            'id'            => 'primary-sidebar',
            'name'          => __( 'Primary Sidebar' ),
            'description'   => __( 'A short description of the sidebar.' ),
            'before_widget' => '',
            'after_widget'  => '',
            'before_title'  => '',
            'after_title'   => '',
        )
    );
    /* Repeat register_sidebar() code for additional sidebars. */
}
add_action( 'widgets_init', 'register_sidebar_locations' );