        <div class="footer-container clearfix">
            <footer class="wrapper">
                <div style="float: left;">
                    <img id="logoF" src="<?php bloginfo('template_url'); ?>/css/images/logoNicky.png">
                </div>
                <div id="disclaimerF">
                    <a href="http://localhost/cmp2/disclaimer/">Disclaimer</a><br>
                    <a href="http://localhost/cmp2/privacy-policy/">Privacy Policy</a>
                </div>
                <div id="contactF">
                    <a href="http://localhost/cmp2/contact/">Contact</a>
                </div>
            </footer>
        </div>

        <script src="//ajax.googleapis.com/ajax/libs/jquery/1.11.2/jquery.min.js"></script>
        <script>window.jQuery || document.write('<script src="js/vendor/jquery-1.11.2.min.js"><\/script>')</script>

        <script src="js/main.js"></script>

        <!-- Google Analytics: change UA-XXXXX-X to be your site's ID. -->
        <script>
            (function(b,o,i,l,e,r){b.GoogleAnalyticsObject=l;b[l]||(b[l]=
            function(){(b[l].q=b[l].q||[]).push(arguments)});b[l].l=+new Date;
            e=o.createElement(i);r=o.getElementsByTagName(i)[0];
            e.src='//www.google-analytics.com/analytics.js';
            r.parentNode.insertBefore(e,r)}(window,document,'script','ga'));
            ga('create','UA-XXXXX-X','auto');ga('send','pageview');
        </script>
        <?php wp_footer(); ?>
    </body>
</html>