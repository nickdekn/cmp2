<?php get_header(); ?>

    <div class="main-container">
        <div class="main wrapper clearfix">

            <article>
                <?php
                if(have_posts())
                {
                    while(have_posts())
                    {
                        the_post();
                        //Print the title and the content of the current post
                        the_title('<h1>', '</h1>');
                        the_content('<p>', '</p>');
                    }
                }
                else
                {
                    echo 'No content available';
                }?>
            </article>

            <?php get_sidebar(); ?>

        </div> <!-- #main -->
    </div> <!-- #main-container -->

<?php get_footer(); ?>